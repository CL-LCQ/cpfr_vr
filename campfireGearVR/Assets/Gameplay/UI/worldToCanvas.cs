﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class worldToCanvas : MonoBehaviour {



    //this is your object that you want to have the UI element hovering over
     GameObject WorldObject;
    public bool isColor;

    //this is the ui element
   public RectTransform UI_Element;

    Canvas mainCanvas;
    Camera gameCamera;

    RectTransform CanvasRect;

    // Use this for initialization
    void Start () {

        gameCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
        mainCanvas = GameObject.FindGameObjectWithTag("canvas").GetComponent<Canvas>();

        //first you need the RectTransform component of your canvas
        CanvasRect = mainCanvas.GetComponent<RectTransform>();

        //then you calculate the position of the UI element
        //0,0 for the canvas is at the center of the screen, whereas WorldToViewPortPoint treats the lower left corner as 0,0. Because of this, you need to subtract the height / width of the canvas * 0.5 to get the correct position.

      

    }
	
	// Update is called once per frame
	void Update () {

        Vector2 ViewportPosition;
        if (isColor == true)
        {

            ViewportPosition = gameCamera.WorldToViewportPoint(this.transform.localPosition);
        }
        else {

             ViewportPosition = gameCamera.WorldToViewportPoint(this.transform.position);
        }

        
        Vector2 WorldObject_ScreenPosition = new Vector2(
        ((ViewportPosition.x * CanvasRect.sizeDelta.x) - (CanvasRect.sizeDelta.x * 0.5f)),
        ((ViewportPosition.y * CanvasRect.sizeDelta.y) - (CanvasRect.sizeDelta.y * 0.5f)));

        //now you can set the position of the ui element
        UI_Element.anchoredPosition = WorldObject_ScreenPosition;

    }
}
