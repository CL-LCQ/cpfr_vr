﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timeGauge : MonoBehaviour {

   public Image fillImg;
    float timeAmt = 2;
    float time;
    public bool isOn = false;
    public float percentage;
    GameObject actionIcon;


    // Use this for initialization
    void Start () {
        fillImg = this.GetComponent<Image>();
        //actionIcon = GameObject.FindGameObjectWithTag("actionIcon");

        reset();

        //actionIcon.SetActive(false);
        


    }
	
	// Update is called once per frame
	void Update () {


  //  this.transform.LookAt(transform.position + Camera.main.transform.rotation * Vector3.forward,
    //Camera.main.transform.rotation * Vector3.up);

        if (isOn == true)
        {
            //actionIcon.SetActive(true);

            if (time > 0)
            {

                time -= Time.deltaTime;
                percentage = 1 - (time / timeAmt);
                fillImg.fillAmount = percentage;



            }
        }
        else {


            //actionIcon.SetActive(false);
        }
       
		
	}

    public void startGauge(float timerLength) {

        //assign the icon
       // actionIcon.GetComponent<Image>().sprite = iconSprite;



        timeAmt = timerLength;

        reset();
        isOn = true;
		print ("startGauge");
    }

    public void stopGauge()
    {
        time = 0.0f;
        isOn = false;
    }

    public void reset()
    {
        
        time = timeAmt;
        fillImg.fillAmount = 0.0f;
    }
}
