﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class uiCollisionTriggers : MonoBehaviour
{
    GameObject circularGauge;
    GameObject playerMesh;
    GameObject actionUI;
    public GameObject actionObject;

    timeGauge gaugeInstance;
    actionScript actionScriptInstance;
   

    Sprite iconSprite;

    bool is3DButton = false;

    AudioSource audioSource;
    bool audiohasPlayed;

    Animator playerAnimator;
    float speed;
    Vector3 mLastPosition;
    float eleapsedTime;

    // Use this for initialization
    void Start()
    {
		gaugeInstance = circularGauge.GetComponent<timeGauge>();
        circularGauge = GameObject.FindGameObjectWithTag("actionTriggerTimer");
        actionUI = GameObject.FindGameObjectWithTag("actionUI");


      

       
    }


    

    // Update is called once per frame
    void Update()
    {
       
        if (gaugeInstance.isOn == true)
        {


            actionUI.transform.position = actionObject.gameObject.transform.position;



            if (gaugeInstance.percentage >= 0.99f)
            {

                //validate the type action of the collided object that isn't player

                actionScriptInstance.actionTrigger(actionObject);


         
                gaugeInstance.reset();



            }




        }

    }


    void OnCollisionEnter2D(Collision2D other)
    {
		print ("coll");

         actionObject = other.gameObject;

        
        if (other.gameObject.tag == "actionTrigger")
        {
			actionScriptInstance = actionObject.GetComponent<actionScript>();
        }


            gaugeInstance.startGauge(actionScriptInstance.triggerTime);
         

     }
    

    void OnCollisionExit2D(Collision2D other)
    {

        gaugeInstance.stopGauge();
        gaugeInstance.reset();

    }

  

}

