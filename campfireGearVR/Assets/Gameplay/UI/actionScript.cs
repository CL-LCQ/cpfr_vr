﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using DG.Tweening;

public class actionScript : MonoBehaviour {


    public actionType[] Type = new actionType[5];
    public int nActions;

    public enum actionType
    {
        none,
        levelLoader,
        travelToZone,
        animationTrigger,
        applyColor,
        getColor,
        scrollpalette,
        clickButton,
        objectSwitch,
        rotate,
        playAudio,
        tunOff,

    }


    public bool playSourceAudio;
    public bool playPlayer2DAudio = true;


    public GameObject objectToSwitch;
    public GameObject objectToOff;

    public bool leftTrigger;
    public bool rightTrigger;
    GameObject camParent;
    public bool hasDelay;

    public string levelTargetname;
    public int levelIndex;
    public float triggerTime;
    public Sprite iconSprite;
    GameObject trackingArea;

    public GameObject showPanel;
    public GameObject hidePanel;


    public Button captureButton;

    public bool locked;

    
     GameObject wings;

    GameObject playerMesh;
    GameObject playerMeshAlpha;
    ParticleSystem playerTrail;


    public float animationSpeed;
    public bool playOnce;
    bool hasPlayed = false;
    public bool isBacknForth;
    AudioSource audioSource;
    public bool loop;
    public float loopTime;
    public GameObject animtarget;

    GameObject colorFX;
    ParticleSystem colorPS;
    Light pointLight;

    public Vector3 colisionPos;

    public GameObject targetTravelPosition;
    public float trackingScale;

    GameObject playerObject;

    // Use this for initialization
    void Start()
    {
    

		camParent = GameObject.FindGameObjectWithTag ("camParent");
       



        if (this.gameObject.GetComponent<AudioSource>())
        {
            audioSource = this.gameObject.GetComponent<AudioSource>();
        }

        

        int size = Type.Length;

        for (int i = 0; i < size; i++)
        {

            if (Type[i] == actionType.levelLoader)
            {

                if (locked == true)
                {
                    //stop the gauge
                    iconSprite = Resources.Load<Sprite>("sprites/actionIcons/actionLocked");
                    triggerTime = 100000000;

                }
                else
                {
                    iconSprite = Resources.Load<Sprite>("sprites/actionIcons/actionExit");
                }

            }

        

            else if (Type[i] == actionType.scrollpalette)
            {
                
             
            }
        }

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void actionTrigger(GameObject other)
    {
        int size = Type.Length;

        for (int i = 0; i < size; i++)
        {

            if (Type[i] == actionType.levelLoader)
            {

                if (locked == false)
                {

                    if (levelIndex != 0)
                    {
                        PlayerPrefs.SetInt("level", levelIndex);


                        if (hasDelay)
                        {

                            StartCoroutine(loadLevelWithDelay("3DpaintScene"));
                        }

                        else
                        {

                            Application.LoadLevel("3DpaintScene");
                        }
                    }


                    else {
                        Application.LoadLevel("mainMenu");

                    }
                }




            }

            else if (Type[i] == actionType.rotate)
            {

                Vector3 newTargetPosition = camParent.transform.rotation.eulerAngles;


                newTargetPosition.y -= 20;


                camParent.transform.DOLocalRotate(newTargetPosition, 1);
            }




            else if (Type[i] == actionType.playAudio)
            {

              //


            }

            else if (Type[i] == actionType.travelToZone)
            {

                //-1.6

                



                camParent.transform.DOMove(targetTravelPosition.transform.position, 2);

               // trackingArea.transform.localPosition = targetTravelPosition.GetComponent<transformTargetParameter>().targetTracingAreaPosition;

                //trackingArea.GetComponent<trackingSystem>().trackingScale = trackingScale;

                //2.54 is 1

               


                //this.gameObject.transform.position = new Vector3(1000, 1000, 1000);







            }

            else if (Type[i] == actionType.applyColor)
            {


                
                other.gameObject.GetComponent<Renderer>().material.color = playerMesh.gameObject.GetComponent<Renderer>().material.color;



                colorPS.startColor = playerMesh.gameObject.GetComponent<Renderer>().material.color;

                colorPS.Play();


            }


            //character gets color
            else if (Type[i] == actionType.getColor)
            {
               
                






            }

            else if (Type[i] == actionType.scrollpalette)
            {

              



            }




            else if (Type[i] == actionType.animationTrigger)
            {
                print("rolling");

                GameObject objectToAnimate;

                if (animtarget)
                {

                    objectToAnimate = animtarget;

                }
                else {

                    objectToAnimate = this.gameObject;
                }




                if (playOnce == false)
                {
                    //switch on or off
                    objectToAnimate.GetComponent<Animator>().enabled = !objectToAnimate.GetComponent<Animator>().enabled;
                }

                else
                {
                    objectToAnimate.GetComponent<Animator>().enabled = true;
                }

                objectToAnimate.GetComponent<Animator>().applyRootMotion = true;
                //objectToAnimate.GetComponent<Animator>().SetFloat("speed", animationSpeed);



                if (isBacknForth == true) {

                    print("reversed anim should play");
                    objectToAnimate.GetComponent<Animator>().Play("reverse");


                    
                }

                else{

                    objectToAnimate.GetComponent<Animator>().Play(0);

                }


            }


            else if (Type[i] == actionType.clickButton)
            {

                captureButton.onClick.Invoke();


            }

            else if (Type[i] == actionType.objectSwitch)
            {

                bool test = objectToSwitch.activeSelf;
              
                test = !test;
                objectToSwitch.SetActive(test);

            }

            else if (Type[i] == actionType.objectSwitch)
            {

                //objectToOff.SetActive(false);
            }



        }

        //  if (this.GetComponent<AudioSource>() && playSourceAudio == true && playPlayer2DAudio ==true)
        if (this.GetComponent<AudioSource>() && playSourceAudio == true )
        {
            audioSource = this.GetComponent<AudioSource>();



            audioSource.Play();

        }
    }

    public void resetme()
    {

        this.transform.localPosition = new Vector3(0, 0, 0);


    }

    public void moveme() {

        this.transform.localPosition = new Vector3(1000, 1000, 1000);
        
    }

    public void resetTriggerPos() {

        this.transform.localPosition = new Vector3(0, 0, 0);
    }
    private IEnumerator moveAway() {

       // yield return new WaitForSeconds(2);
        this.transform.localPosition = new Vector3(0, 0, 0);


        yield return null;

    }

    private IEnumerator loadLevelWithDelay(string lvlName)
    {

         yield return new WaitForSeconds(1);


        Application.LoadLevel(lvlName);

        

    }


    private IEnumerator animCoroutine(Animator animator)
    {
        animator.enabled = true;
        animator.Play(0);
        yield return new WaitForSeconds(loopTime);
        animator.enabled = false;


        yield return null;
    }
}
