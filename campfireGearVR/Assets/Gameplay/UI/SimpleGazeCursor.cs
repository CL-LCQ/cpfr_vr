﻿using UnityEngine;
using System.Collections;
using System;

public class SimpleGazeCursor : MonoBehaviour {
    public Camera viewCamera;
    public GameObject cursorPrefab;
    public float maxCursorDistance = 30;

	GameObject circularGauge;
	public GameObject actionObject;
	GameObject previousActionObject;
	GameObject actionUI;
	timeGauge gaugeInstance;
	actionScript actionScriptInstance;


    private GameObject cursorInstance;

	// Use this for initialization
	void Start () {
        cursorInstance = Instantiate(cursorPrefab);

		circularGauge = GameObject.FindGameObjectWithTag("actionTriggerTimer");
		actionUI = GameObject.FindGameObjectWithTag("actionUI");
		gaugeInstance = circularGauge.GetComponent<timeGauge>();

	}
	
	// Update is called once per frame
	void Update () {
        UpdateCursor();



		if (gaugeInstance.isOn == true)
		{


			//actionUI.transform.position = actionObject.gameObject.transform.position;



			if (gaugeInstance.percentage >= 0.99f)
			{

				//validate the type action of the collided object that isn't player

				actionScriptInstance.actionTrigger(actionObject);



				gaugeInstance.reset();



			}




		}
	}

    /// <summary>
    /// Updates the cursor based on what the camera is pointed at.
    /// </summary>
    private void UpdateCursor()
    {
        // Create a gaze ray pointing forward from the camera
        Ray ray = new Ray(viewCamera.transform.position, viewCamera.transform.rotation * Vector3.forward);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity))
        {

			actionObject = hit.collider.gameObject;


			if (previousActionObject == null) {
			
				previousActionObject = actionObject;

				if (actionObject.tag == "actionTrigger"){

					print ("looking at a new trigger");

					actionScriptInstance = actionObject.GetComponent<actionScript>();

					gaugeInstance.startGauge(actionScriptInstance.triggerTime);
				}
			}


			 if (previousActionObject == actionObject) {

				//looking at the same

				//do nothing
				print ("looking at the same trigger");

			}

			else if (previousActionObject != actionObject) {



				//looking at a new object, start gauge
				if (actionObject.tag == "actionTrigger"){

					print ("looking at a new trigger");

					actionScriptInstance = actionObject.GetComponent<actionScript>();

					gaugeInstance.startGauge(actionScriptInstance.triggerTime);
				}



			}

			previousActionObject = actionObject;


            // If the ray hits something, set the position to the hit point and rotate based on the normal vector of the hit
            cursorInstance.transform.position = hit.point;
           // cursorInstance.transform.rotation = Quaternion.FromToRotation(Vector3.up, hit.normal);






        }
        else
        {
			previousActionObject = null;

			print ("looking out");
            // If the ray doesn't hit anything, set the position to the maxCursorDistance and rotate to point away from the camera
            cursorInstance.transform.position = ray.origin + ray.direction.normalized * maxCursorDistance;
            //cursorInstance.transform.rotation = Quaternion.FromToRotation(Vector3.up, -ray.direction);

			gaugeInstance.stopGauge();
			gaugeInstance.reset();
        }
    }
}
