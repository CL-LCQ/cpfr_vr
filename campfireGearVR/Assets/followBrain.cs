﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;

public class followBrain : MonoBehaviour {

	public GameObject brain;
	public bool followFromStart;
	//public TimelineAsset mainTimeline;
	public GameObject director;
	PlayableDirector currentDirector;
	public float startFollow;

	public Vector3 offset;
	public bool rotate = false;

	bool follow = false;

	public Transform lookAtTarget;


	// Use this for initialization
	void Start () {

		if (followFromStart == true) {

			follow = true;

		} 

		else {
			follow = false;

			currentDirector = director.GetComponent<PlayableDirector>();
		}
	}
	
	// Update is called once per frame
	void Update () {

		if (follow == true) {

			print ("following");	

			this.transform.position = brain.transform.position + offset;
			if (lookAtTarget) {
				transform.LookAt(lookAtTarget);
			}


			if (rotate == true) {
			
				this.transform.eulerAngles = brain.transform.eulerAngles;
			}
		} 



		else if (currentDirector.time > startFollow) {

			print ("nowFollow");	
			follow = true;

			this.gameObject.GetComponent<Animator> ().enabled = false;
			}

		
	}



}
