﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Timeline;
using UnityEngine.Playables;
using UnityEngine.SceneManagement;

public class timeLoader : MonoBehaviour {

	public GameObject director;
	PlayableDirector currentDirector;
	public float eventTime;

	// Use this for initialization
	void Start () {
		currentDirector = director.GetComponent<PlayableDirector>();
	}
	
	// Update is called once per frame
	void Update () {

		if (currentDirector.time > eventTime) {

			SceneManager.LoadScene ("TEMPLE");
		}
		
	}
}
