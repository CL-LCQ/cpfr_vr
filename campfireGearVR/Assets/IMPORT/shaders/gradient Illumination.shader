// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:33162,y:32492,varname:node_4013,prsc:2|emission-3440-OUT,custl-8581-OUT;n:type:ShaderForge.SFN_Color,id:1304,x:32236,y:32397,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1304,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Lerp,id:8833,x:32592,y:32595,varname:node_8833,prsc:2|A-1304-RGB,B-4313-RGB,T-7065-OUT;n:type:ShaderForge.SFN_Color,id:4313,x:32088,y:32569,ptovrint:False,ptlb:Color_copy,ptin:_Color_copy,varname:_Color_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.3338124,c2:0.6691177,c3:0.2705991,c4:1;n:type:ShaderForge.SFN_TexCoord,id:2484,x:32107,y:32819,varname:node_2484,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_LightColor,id:5732,x:32520,y:32875,varname:node_5732,prsc:2;n:type:ShaderForge.SFN_Multiply,id:8581,x:32787,y:32876,varname:node_8581,prsc:2|A-4621-OUT,B-8833-OUT;n:type:ShaderForge.SFN_Slider,id:3022,x:32768,y:32460,ptovrint:False,ptlb:emissive power,ptin:_emissivepower,varname:node_3022,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Multiply,id:3440,x:32926,y:32619,varname:node_3440,prsc:2|A-8833-OUT,B-3022-OUT;n:type:ShaderForge.SFN_Multiply,id:4621,x:32685,y:33030,varname:node_4621,prsc:2|A-5732-RGB,B-9538-OUT;n:type:ShaderForge.SFN_Slider,id:9538,x:32378,y:33088,ptovrint:False,ptlb:light color power,ptin:_lightcolorpower,varname:node_9538,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:0,max:1;n:type:ShaderForge.SFN_SwitchProperty,id:7065,x:32333,y:32697,ptovrint:False,ptlb:vertical,ptin:_vertical,varname:node_7065,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,on:False|A-2484-V,B-2484-U;proporder:1304-4313-3022-9538-7065;pass:END;sub:END;*/

Shader "Shader Forge/gradient Illumination" {
    Properties {
        _Color ("Color", Color) = (1,0,0,1)
        _Color_copy ("Color_copy", Color) = (0.3338124,0.6691177,0.2705991,1)
        _emissivepower ("emissive power", Range(0, 1)) = 1
        _lightcolorpower ("light color power", Range(0, 1)) = 0
        [MaterialToggle] _vertical ("vertical", Float ) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform float4 _Color_copy;
            uniform float _emissivepower;
            uniform float _lightcolorpower;
            uniform fixed _vertical;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                LIGHTING_COORDS(1,2)
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
////// Emissive:
                float3 node_8833 = lerp(_Color.rgb,_Color_copy.rgb,lerp( i.uv0.g, i.uv0.r, _vertical ));
                float3 emissive = (node_8833*_emissivepower);
                float3 finalColor = emissive + ((_LightColor0.rgb*_lightcolorpower)*node_8833);
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform float4 _Color_copy;
            uniform float _emissivepower;
            uniform float _lightcolorpower;
            uniform fixed _vertical;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                LIGHTING_COORDS(1,2)
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float3 node_8833 = lerp(_Color.rgb,_Color_copy.rgb,lerp( i.uv0.g, i.uv0.r, _vertical ));
                float3 finalColor = ((_LightColor0.rgb*_lightcolorpower)*node_8833);
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
