// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:0,bdst:1,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:32975,y:32581,varname:node_4013,prsc:2|diff-6116-OUT,alpha-2386-VOUT,clip-2386-VOUT;n:type:ShaderForge.SFN_Color,id:1304,x:32489,y:32431,ptovrint:False,ptlb:ColorA,ptin:_ColorA,varname:node_1304,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Tex2d,id:835,x:32350,y:32827,varname:node_835,prsc:2,tex:7abf6953ff06e9c4fbf2ed00e1939042,ntxv:0,isnm:False|UVIN-7395-UVOUT,TEX-683-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:683,x:32250,y:33046,ptovrint:False,ptlb:node_683,ptin:_node_683,varname:node_683,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,tex:7abf6953ff06e9c4fbf2ed00e1939042,ntxv:0,isnm:False;n:type:ShaderForge.SFN_TexCoord,id:7395,x:32176,y:32779,varname:node_7395,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_RgbToHsv,id:2386,x:32615,y:32943,varname:node_2386,prsc:2|IN-835-RGB;n:type:ShaderForge.SFN_Color,id:723,x:32471,y:32586,ptovrint:False,ptlb:ColorB,ptin:_ColorB,varname:node_723,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Lerp,id:6116,x:32745,y:32580,varname:node_6116,prsc:2|A-723-RGB,B-1304-RGB,T-7395-U;proporder:1304-683-723;pass:END;sub:END;*/

Shader "Shader Forge/brush2" {
    Properties {
        _ColorA ("ColorA", Color) = (1,1,1,1)
        _node_683 ("node_683", 2D) = "white" {}
        _ColorB ("ColorB", Color) = (0.5,0.5,0.5,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles metal 
            #pragma target 2.0
            uniform float4 _LightColor0;
            uniform float4 _ColorA;
            uniform sampler2D _node_683; uniform float4 _node_683_ST;
            uniform float4 _ColorB;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float4 node_835 = tex2D(_node_683,TRANSFORM_TEX(i.uv0, _node_683));
                float4 node_2386_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_2386_p = lerp(float4(float4(node_835.rgb,0.0).zy, node_2386_k.wz), float4(float4(node_835.rgb,0.0).yz, node_2386_k.xy), step(float4(node_835.rgb,0.0).z, float4(node_835.rgb,0.0).y));
                float4 node_2386_q = lerp(float4(node_2386_p.xyw, float4(node_835.rgb,0.0).x), float4(float4(node_835.rgb,0.0).x, node_2386_p.yzx), step(node_2386_p.x, float4(node_835.rgb,0.0).x));
                float node_2386_d = node_2386_q.x - min(node_2386_q.w, node_2386_q.y);
                float node_2386_e = 1.0e-10;
                float3 node_2386 = float3(abs(node_2386_q.z + (node_2386_q.w - node_2386_q.y) / (6.0 * node_2386_d + node_2386_e)), node_2386_d / (node_2386_q.x + node_2386_e), node_2386_q.x);;
                clip(node_2386.b - 0.5);
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuseColor = lerp(_ColorB.rgb,_ColorA.rgb,i.uv0.r);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor,node_2386.b);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles metal 
            #pragma target 2.0
            uniform float4 _LightColor0;
            uniform float4 _ColorA;
            uniform sampler2D _node_683; uniform float4 _node_683_ST;
            uniform float4 _ColorB;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float4 node_835 = tex2D(_node_683,TRANSFORM_TEX(i.uv0, _node_683));
                float4 node_2386_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_2386_p = lerp(float4(float4(node_835.rgb,0.0).zy, node_2386_k.wz), float4(float4(node_835.rgb,0.0).yz, node_2386_k.xy), step(float4(node_835.rgb,0.0).z, float4(node_835.rgb,0.0).y));
                float4 node_2386_q = lerp(float4(node_2386_p.xyw, float4(node_835.rgb,0.0).x), float4(float4(node_835.rgb,0.0).x, node_2386_p.yzx), step(node_2386_p.x, float4(node_835.rgb,0.0).x));
                float node_2386_d = node_2386_q.x - min(node_2386_q.w, node_2386_q.y);
                float node_2386_e = 1.0e-10;
                float3 node_2386 = float3(abs(node_2386_q.z + (node_2386_q.w - node_2386_q.y) / (6.0 * node_2386_d + node_2386_e)), node_2386_d / (node_2386_q.x + node_2386_e), node_2386_q.x);;
                clip(node_2386.b - 0.5);
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 diffuseColor = lerp(_ColorB.rgb,_ColorA.rgb,i.uv0.r);
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                fixed4 finalRGBA = fixed4(finalColor * node_2386.b,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles metal 
            #pragma target 2.0
            uniform sampler2D _node_683; uniform float4 _node_683_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
                float2 uv0 : TEXCOORD1;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                float4 node_835 = tex2D(_node_683,TRANSFORM_TEX(i.uv0, _node_683));
                float4 node_2386_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_2386_p = lerp(float4(float4(node_835.rgb,0.0).zy, node_2386_k.wz), float4(float4(node_835.rgb,0.0).yz, node_2386_k.xy), step(float4(node_835.rgb,0.0).z, float4(node_835.rgb,0.0).y));
                float4 node_2386_q = lerp(float4(node_2386_p.xyw, float4(node_835.rgb,0.0).x), float4(float4(node_835.rgb,0.0).x, node_2386_p.yzx), step(node_2386_p.x, float4(node_835.rgb,0.0).x));
                float node_2386_d = node_2386_q.x - min(node_2386_q.w, node_2386_q.y);
                float node_2386_e = 1.0e-10;
                float3 node_2386 = float3(abs(node_2386_q.z + (node_2386_q.w - node_2386_q.y) / (6.0 * node_2386_d + node_2386_e)), node_2386_d / (node_2386_q.x + node_2386_e), node_2386_q.x);;
                clip(node_2386.b - 0.5);
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
