// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:False,igpj:False,qofs:0,qpre:0,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:9361,x:34533,y:32776,varname:node_9361,prsc:2|emission-753-OUT,custl-6262-OUT;n:type:ShaderForge.SFN_LightVector,id:4365,x:32419,y:32825,varname:node_4365,prsc:2;n:type:ShaderForge.SFN_Dot,id:1266,x:32722,y:32704,varname:node_1266,prsc:2,dt:1|A-4365-OUT,B-351-OUT;n:type:ShaderForge.SFN_Multiply,id:7214,x:32989,y:32802,varname:node_7214,prsc:2|A-1266-OUT,B-9000-OUT;n:type:ShaderForge.SFN_OneMinus,id:947,x:32971,y:33019,varname:node_947,prsc:2|IN-9000-OUT;n:type:ShaderForge.SFN_Vector1,id:9000,x:32602,y:33093,varname:node_9000,prsc:2,v1:0.1;n:type:ShaderForge.SFN_Add,id:4522,x:33201,y:32967,varname:node_4522,prsc:2|A-7214-OUT,B-947-OUT;n:type:ShaderForge.SFN_Vector4,id:9159,x:33339,y:33104,varname:node_9159,prsc:2,v1:1,v2:1,v3:1,v4:1;n:type:ShaderForge.SFN_LightAttenuation,id:234,x:33451,y:32904,varname:node_234,prsc:2;n:type:ShaderForge.SFN_ComponentMask,id:9522,x:33657,y:32904,varname:node_9522,prsc:2,cc1:0,cc2:-1,cc3:-1,cc4:-1|IN-234-OUT;n:type:ShaderForge.SFN_Lerp,id:3871,x:33827,y:32587,varname:node_3871,prsc:2|A-7683-RGB,B-9904-RGB,T-9522-OUT;n:type:ShaderForge.SFN_Color,id:7683,x:33425,y:32469,ptovrint:False,ptlb:Shadow,ptin:_Shadow,varname:node_7683,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1130515,c2:0.1690542,c3:0.375,c4:1;n:type:ShaderForge.SFN_Color,id:9904,x:33425,y:32653,ptovrint:False,ptlb:Light,ptin:_Light,varname:_node_7683_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.7205882,c2:0.2635335,c3:0.09537198,c4:1;n:type:ShaderForge.SFN_Fresnel,id:1336,x:33453,y:33209,varname:node_1336,prsc:2|EXP-8908-OUT;n:type:ShaderForge.SFN_Vector1,id:7841,x:33290,y:33413,varname:node_7841,prsc:2,v1:0.01;n:type:ShaderForge.SFN_Multiply,id:6989,x:33713,y:33099,varname:node_6989,prsc:2|A-9159-OUT,B-8832-OUT;n:type:ShaderForge.SFN_Add,id:8832,x:33607,y:33326,varname:node_8832,prsc:2|A-1336-OUT,B-7841-OUT;n:type:ShaderForge.SFN_Slider,id:4562,x:32919,y:33323,ptovrint:False,ptlb:Fresnel,ptin:_Fresnel,varname:node_4562,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:-15,cur:-5.981579,max:1;n:type:ShaderForge.SFN_OneMinus,id:8908,x:33263,y:33235,varname:node_8908,prsc:2|IN-4562-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:3145,x:33310,y:32072,ptovrint:False,ptlb:Pattern,ptin:_Pattern,varname:node_3145,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_TexCoord,id:5628,x:33073,y:31793,varname:node_5628,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Tex2d,id:5929,x:33573,y:31897,varname:node_5929,prsc:2,ntxv:0,isnm:False|UVIN-3129-UVOUT,TEX-3145-TEX;n:type:ShaderForge.SFN_UVTile,id:3129,x:33379,y:31843,varname:node_3129,prsc:2|UVIN-5628-UVOUT,WDT-1099-OUT,HGT-1099-OUT,TILE-1099-OUT;n:type:ShaderForge.SFN_ValueProperty,id:1099,x:32926,y:32003,ptovrint:False,ptlb:Tile,ptin:_Tile,varname:node_1099,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,v1:1;n:type:ShaderForge.SFN_Vector1,id:2395,x:33811,y:32001,varname:node_2395,prsc:2,v1:1.5;n:type:ShaderForge.SFN_Multiply,id:1406,x:33623,y:32122,varname:node_1406,prsc:2|A-5929-RGB,B-2395-OUT;n:type:ShaderForge.SFN_Clamp01,id:1511,x:33811,y:32140,varname:node_1511,prsc:2|IN-1406-OUT;n:type:ShaderForge.SFN_HalfVector,id:3459,x:32444,y:32232,varname:node_3459,prsc:2;n:type:ShaderForge.SFN_Dot,id:2098,x:32674,y:32276,varname:node_2098,prsc:2,dt:1|A-3459-OUT,B-351-OUT;n:type:ShaderForge.SFN_Power,id:6014,x:32862,y:32276,varname:node_6014,prsc:2|VAL-2098-OUT,EXP-6448-OUT;n:type:ShaderForge.SFN_Add,id:4749,x:34052,y:32475,varname:node_4749,prsc:2|A-5382-OUT,B-3871-OUT;n:type:ShaderForge.SFN_NormalVector,id:351,x:32419,y:32589,prsc:2,pt:True;n:type:ShaderForge.SFN_LightColor,id:480,x:33919,y:32729,varname:node_480,prsc:2;n:type:ShaderForge.SFN_Multiply,id:6262,x:34230,y:32826,varname:node_6262,prsc:2|A-3014-OUT,B-480-RGB,C-9651-OUT;n:type:ShaderForge.SFN_Multiply,id:2097,x:33122,y:32271,varname:node_2097,prsc:2|A-6014-OUT,B-3642-OUT;n:type:ShaderForge.SFN_OneMinus,id:204,x:34008,y:32140,varname:node_204,prsc:2|IN-1511-OUT;n:type:ShaderForge.SFN_Slider,id:6448,x:32536,y:32044,ptovrint:False,ptlb:Glossiness,ptin:_Glossiness,varname:node_6448,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:16.99945,max:30;n:type:ShaderForge.SFN_Slider,id:3642,x:32841,y:32564,ptovrint:False,ptlb:Specular,ptin:_Specular,varname:_Glossiness_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:10,max:10;n:type:ShaderForge.SFN_Color,id:1188,x:33228,y:32508,ptovrint:False,ptlb:specular color,ptin:_specularcolor,varname:node_1188,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:7536,x:33425,y:32269,varname:node_7536,prsc:2|A-2097-OUT,B-1188-RGB;n:type:ShaderForge.SFN_Multiply,id:1172,x:34030,y:33171,varname:node_1172,prsc:2|A-6989-OUT,B-9187-RGB;n:type:ShaderForge.SFN_Color,id:9187,x:33786,y:33326,ptovrint:False,ptlb:Fresnel Color,ptin:_FresnelColor,varname:node_9187,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:5382,x:33827,y:32436,varname:node_5382,prsc:2|A-204-OUT,B-7536-OUT;n:type:ShaderForge.SFN_Multiply,id:753,x:34259,y:33012,varname:node_753,prsc:2|A-9904-RGB,B-1172-OUT;n:type:ShaderForge.SFN_Multiply,id:9651,x:33919,y:32868,varname:node_9651,prsc:2|A-9904-RGB,B-1266-OUT;n:type:ShaderForge.SFN_Tex2dAsset,id:7201,x:33950,y:31701,ptovrint:False,ptlb:Diffuse image,ptin:_Diffuseimage,varname:node_7201,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5242,x:34141,y:31869,varname:node_5242,prsc:2,ntxv:0,isnm:False|UVIN-3129-UVOUT,TEX-7201-TEX;n:type:ShaderForge.SFN_Multiply,id:3014,x:34265,y:32216,varname:node_3014,prsc:2|A-5242-RGB,B-4749-OUT;proporder:7683-9904-9187-4562-3145-1099-6448-1188-3642-7201;pass:END;sub:END;*/

Shader "Shader Forge/customLightingexample01" {
    Properties {
        _Shadow ("Shadow", Color) = (0.1130515,0.1690542,0.375,1)
        _Light ("Light", Color) = (0.7205882,0.2635335,0.09537198,1)
        _FresnelColor ("Fresnel Color", Color) = (0.5,0.5,0.5,1)
        _Fresnel ("Fresnel", Range(-15, 1)) = -5.981579
        _Pattern ("Pattern", 2D) = "white" {}
        _Tile ("Tile", Float ) = 1
        _Glossiness ("Glossiness", Range(1, 30)) = 16.99945
        _specularcolor ("specular color", Color) = (1,1,1,1)
        _Specular ("Specular", Range(1, 10)) = 10
        _Diffuseimage ("Diffuse image", 2D) = "white" {}
    }
    SubShader {
        Tags {
            "Queue"="Background"
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            uniform float4 _Shadow;
            uniform float4 _Light;
            uniform float _Fresnel;
            uniform sampler2D _Pattern; uniform float4 _Pattern_ST;
            uniform float _Tile;
            uniform float _Glossiness;
            uniform float _Specular;
            uniform float4 _specularcolor;
            uniform float4 _FresnelColor;
            uniform sampler2D _Diffuseimage; uniform float4 _Diffuseimage_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
////// Emissive:
                float3 emissive = (_Light.rgb*((float4(1,1,1,1)*(pow(1.0-max(0,dot(normalDirection, viewDirection)),(1.0 - _Fresnel))+0.01))*_FresnelColor.rgb)).rgb;
                float2 node_3129_tc_rcp = float2(1.0,1.0)/float2( _Tile, _Tile );
                float node_3129_ty = floor(_Tile * node_3129_tc_rcp.x);
                float node_3129_tx = _Tile - _Tile * node_3129_ty;
                float2 node_3129 = (i.uv0 + float2(node_3129_tx, node_3129_ty)) * node_3129_tc_rcp;
                float4 node_5242 = tex2D(_Diffuseimage,TRANSFORM_TEX(node_3129, _Diffuseimage));
                float4 node_5929 = tex2D(_Pattern,TRANSFORM_TEX(node_3129, _Pattern));
                float node_1266 = max(0,dot(lightDirection,normalDirection));
                float3 finalColor = emissive + ((node_5242.rgb*(((1.0 - saturate((node_5929.rgb*1.5)))*((pow(max(0,dot(halfDirection,normalDirection)),_Glossiness)*_Specular)*_specularcolor.rgb))+lerp(_Shadow.rgb,_Light.rgb,attenuation.r)))*_LightColor0.rgb*(_Light.rgb*node_1266));
                fixed4 finalRGBA = fixed4(finalColor,1);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            uniform float4 _Shadow;
            uniform float4 _Light;
            uniform float _Fresnel;
            uniform sampler2D _Pattern; uniform float4 _Pattern_ST;
            uniform float _Tile;
            uniform float _Glossiness;
            uniform float _Specular;
            uniform float4 _specularcolor;
            uniform float4 _FresnelColor;
            uniform sampler2D _Diffuseimage; uniform float4 _Diffuseimage_ST;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
                float3 halfDirection = normalize(viewDirection+lightDirection);
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float2 node_3129_tc_rcp = float2(1.0,1.0)/float2( _Tile, _Tile );
                float node_3129_ty = floor(_Tile * node_3129_tc_rcp.x);
                float node_3129_tx = _Tile - _Tile * node_3129_ty;
                float2 node_3129 = (i.uv0 + float2(node_3129_tx, node_3129_ty)) * node_3129_tc_rcp;
                float4 node_5242 = tex2D(_Diffuseimage,TRANSFORM_TEX(node_3129, _Diffuseimage));
                float4 node_5929 = tex2D(_Pattern,TRANSFORM_TEX(node_3129, _Pattern));
                float node_1266 = max(0,dot(lightDirection,normalDirection));
                float3 finalColor = ((node_5242.rgb*(((1.0 - saturate((node_5929.rgb*1.5)))*((pow(max(0,dot(halfDirection,normalDirection)),_Glossiness)*_Specular)*_specularcolor.rgb))+lerp(_Shadow.rgb,_Light.rgb,attenuation.r)))*_LightColor0.rgb*(_Light.rgb*node_1266));
                fixed4 finalRGBA = fixed4(finalColor * 1,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
