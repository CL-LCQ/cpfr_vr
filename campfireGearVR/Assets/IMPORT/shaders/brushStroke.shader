// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:1,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:2,bsrc:3,bdst:7,dpts:2,wrdp:False,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:True,aust:True,igpj:True,qofs:0,qpre:3,rntp:2,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:4013,x:32977,y:32656,varname:node_4013,prsc:2|diff-4831-OUT,emission-3358-OUT,alpha-5910-VOUT;n:type:ShaderForge.SFN_Color,id:1304,x:32345,y:32310,ptovrint:False,ptlb:Color,ptin:_Color,varname:node_1304,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.03448272,c2:1,c3:0,c4:1;n:type:ShaderForge.SFN_Tex2d,id:9873,x:32388,y:32914,varname:node_9873,prsc:2,ntxv:0,isnm:False|UVIN-6986-UVOUT,TEX-3505-TEX;n:type:ShaderForge.SFN_Tex2dAsset,id:3505,x:32206,y:33066,ptovrint:False,ptlb:Texture,ptin:_Texture,varname:node_3505,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,ntxv:0,isnm:False;n:type:ShaderForge.SFN_TexCoord,id:6986,x:32206,y:32821,varname:node_6986,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_RgbToHsv,id:5910,x:32758,y:32871,varname:node_5910,prsc:2|IN-7362-OUT;n:type:ShaderForge.SFN_Multiply,id:3358,x:32798,y:32713,varname:node_3358,prsc:2|A-5359-RGB,B-6549-OUT;n:type:ShaderForge.SFN_Slider,id:6549,x:32429,y:32795,ptovrint:False,ptlb:emissive power,ptin:_emissivepower,varname:node_6549,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:0,cur:1,max:1;n:type:ShaderForge.SFN_Lerp,id:4831,x:32600,y:32433,varname:node_4831,prsc:2|A-1304-RGB,B-5981-RGB,T-6986-U;n:type:ShaderForge.SFN_Color,id:5981,x:32345,y:32483,ptovrint:False,ptlb:Color B,ptin:_ColorB,varname:_Color_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.9632353,c2:0.4887003,c3:0.4887003,c4:1;n:type:ShaderForge.SFN_Color,id:5359,x:32650,y:32608,ptovrint:False,ptlb:Emissive Color,ptin:_EmissiveColor,varname:node_5359,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Color,id:1198,x:32452,y:33105,ptovrint:False,ptlb:Alpha,ptin:_Alpha,varname:node_1198,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Multiply,id:7362,x:32736,y:33127,varname:node_7362,prsc:2|A-9873-RGB,B-1198-RGB;n:type:ShaderForge.SFN_LightAttenuation,id:7423,x:31870,y:32570,varname:node_7423,prsc:2;proporder:1304-5981-3505-6549-5359-1198;pass:END;sub:END;*/

Shader "Shader Forge/brushStroke" {
    Properties {
        _Color ("Color", Color) = (0.03448272,1,0,1)
        _ColorB ("Color B", Color) = (0.9632353,0.4887003,0.4887003,1)
        _Texture ("Texture", 2D) = "white" {}
        _emissivepower ("emissive power", Range(0, 1)) = 1
        _EmissiveColor ("Emissive Color", Color) = (0.5,0.5,0.5,1)
        _Alpha ("Alpha", Color) = (1,1,1,1)
        [HideInInspector]_Cutoff ("Alpha cutoff", Range(0,1)) = 0.5
    }
    SubShader {
        Tags {
            "IgnoreProjector"="True"
            "Queue"="Transparent"
            "RenderType"="Transparent"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            Blend SrcAlpha OneMinusSrcAlpha
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _emissivepower;
            uniform float4 _ColorB;
            uniform float4 _EmissiveColor;
            uniform float4 _Alpha;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                UNITY_FOG_COORDS(3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = 1;
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 indirectDiffuse = float3(0,0,0);
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                float3 diffuseColor = lerp(_Color.rgb,_ColorB.rgb,i.uv0.r);
                float3 diffuse = (directDiffuse + indirectDiffuse) * diffuseColor;
////// Emissive:
                float3 emissive = (_EmissiveColor.rgb*_emissivepower);
/// Final Color:
                float3 finalColor = diffuse + emissive;
                float4 node_9873 = tex2D(_Texture,TRANSFORM_TEX(i.uv0, _Texture));
                float3 node_7362 = (node_9873.rgb*_Alpha.rgb);
                float4 node_5910_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_5910_p = lerp(float4(float4(node_7362,0.0).zy, node_5910_k.wz), float4(float4(node_7362,0.0).yz, node_5910_k.xy), step(float4(node_7362,0.0).z, float4(node_7362,0.0).y));
                float4 node_5910_q = lerp(float4(node_5910_p.xyw, float4(node_7362,0.0).x), float4(float4(node_7362,0.0).x, node_5910_p.yzx), step(node_5910_p.x, float4(node_7362,0.0).x));
                float node_5910_d = node_5910_q.x - min(node_5910_q.w, node_5910_q.y);
                float node_5910_e = 1.0e-10;
                float3 node_5910 = float3(abs(node_5910_q.z + (node_5910_q.w - node_5910_q.y) / (6.0 * node_5910_d + node_5910_e)), node_5910_d / (node_5910_q.x + node_5910_e), node_5910_q.x);;
                fixed4 finalRGBA = fixed4(finalColor,node_5910.b);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            Cull Off
            ZWrite Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdadd
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            uniform float4 _LightColor0;
            uniform float4 _Color;
            uniform sampler2D _Texture; uniform float4 _Texture_ST;
            uniform float _emissivepower;
            uniform float4 _ColorB;
            uniform float4 _EmissiveColor;
            uniform float4 _Alpha;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
                UNITY_FOG_COORDS(5)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                UNITY_TRANSFER_FOG(o,o.pos);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                i.normalDir = normalize(i.normalDir);
                i.normalDir *= faceSign;
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - i.posWorld.xyz,_WorldSpaceLightPos0.w));
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                float3 diffuseColor = lerp(_Color.rgb,_ColorB.rgb,i.uv0.r);
                float3 diffuse = directDiffuse * diffuseColor;
/// Final Color:
                float3 finalColor = diffuse;
                float4 node_9873 = tex2D(_Texture,TRANSFORM_TEX(i.uv0, _Texture));
                float3 node_7362 = (node_9873.rgb*_Alpha.rgb);
                float4 node_5910_k = float4(0.0, -1.0 / 3.0, 2.0 / 3.0, -1.0);
                float4 node_5910_p = lerp(float4(float4(node_7362,0.0).zy, node_5910_k.wz), float4(float4(node_7362,0.0).yz, node_5910_k.xy), step(float4(node_7362,0.0).z, float4(node_7362,0.0).y));
                float4 node_5910_q = lerp(float4(node_5910_p.xyw, float4(node_7362,0.0).x), float4(float4(node_7362,0.0).x, node_5910_p.yzx), step(node_5910_p.x, float4(node_7362,0.0).x));
                float node_5910_d = node_5910_q.x - min(node_5910_q.w, node_5910_q.y);
                float node_5910_e = 1.0e-10;
                float3 node_5910 = float3(abs(node_5910_q.z + (node_5910_q.w - node_5910_q.y) / (6.0 * node_5910_d + node_5910_e)), node_5910_d / (node_5910_q.x + node_5910_e), node_5910_q.x);;
                fixed4 finalRGBA = fixed4(finalColor * node_5910.b,0);
                UNITY_APPLY_FOG(i.fogCoord, finalRGBA);
                return finalRGBA;
            }
            ENDCG
        }
        Pass {
            Name "ShadowCaster"
            Tags {
                "LightMode"="ShadowCaster"
            }
            Offset 1, 1
            Cull Off
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_SHADOWCASTER
            #include "UnityCG.cginc"
            #include "Lighting.cginc"
            #pragma fragmentoption ARB_precision_hint_fastest
            #pragma multi_compile_shadowcaster
            #pragma multi_compile_fog
            #pragma only_renderers d3d9 d3d11 glcore gles gles3 metal 
            #pragma target 2.0
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                V2F_SHADOW_CASTER;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_SHADOW_CASTER(o)
                return o;
            }
            float4 frag(VertexOutput i, float facing : VFACE) : COLOR {
                float isFrontFace = ( facing >= 0 ? 1 : 0 );
                float faceSign = ( facing >= 0 ? 1 : -1 );
                SHADOW_CASTER_FRAGMENT(i)
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
