// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:0,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:True,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:32947,y:32677,varname:node_3138,prsc:2|emission-5256-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:31851,y:32420,ptovrint:False,ptlb:ColorMid,ptin:_ColorMid,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_Color,id:4976,x:31697,y:32556,ptovrint:False,ptlb:ColorBottom,ptin:_ColorBottom,varname:node_4976,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:1,c3:1,c4:1;n:type:ShaderForge.SFN_Lerp,id:5924,x:32338,y:32530,varname:node_5924,prsc:2|A-4976-RGB,B-7241-RGB,T-7431-OUT;n:type:ShaderForge.SFN_TexCoord,id:1399,x:31539,y:32700,varname:node_1399,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_Lerp,id:5256,x:32652,y:32776,varname:node_5256,prsc:2|A-5924-OUT,B-7164-RGB,T-520-OUT;n:type:ShaderForge.SFN_Color,id:7164,x:32289,y:32794,ptovrint:False,ptlb:TOP,ptin:_TOP,varname:node_7164,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.1415441,c2:0.1796145,c3:0.5661765,c4:1;n:type:ShaderForge.SFN_TexCoord,id:2391,x:32228,y:32984,varname:node_2391,prsc:2,uv:0,uaff:False;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:4634,x:32498,y:33126,varname:node_4634,prsc:2|IN-2391-V,IMIN-3281-OUT,IMAX-5745-OUT,OMIN-7580-OUT,OMAX-2654-OUT;n:type:ShaderForge.SFN_Vector1,id:3281,x:32252,y:33157,varname:node_3281,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:5745,x:32173,y:33207,varname:node_5745,prsc:2,v1:1;n:type:ShaderForge.SFN_Vector1,id:7580,x:32272,y:33274,varname:node_7580,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:3833,x:31516,y:32862,varname:node_3833,prsc:2,v1:0;n:type:ShaderForge.SFN_Vector1,id:7715,x:31569,y:32917,varname:node_7715,prsc:2,v1:1;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:7431,x:32080,y:32725,varname:node_7431,prsc:2|IN-1399-V,IMIN-3833-OUT,IMAX-7715-OUT,OMIN-1076-OUT,OMAX-8752-OUT;n:type:ShaderForge.SFN_Vector1,id:1076,x:31539,y:32987,varname:node_1076,prsc:2,v1:0;n:type:ShaderForge.SFN_Slider,id:8752,x:31626,y:33025,ptovrint:False,ptlb:Bottom Color,ptin:_BottomColor,varname:_TOPcolorposition_copy,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,min:1,cur:1,max:0;n:type:ShaderForge.SFN_Multiply,id:520,x:32678,y:33126,varname:node_520,prsc:2|A-4634-OUT,B-5383-OUT;n:type:ShaderForge.SFN_Vector1,id:2654,x:32272,y:33363,varname:node_2654,prsc:2,v1:1;n:type:ShaderForge.SFN_RemapRangeAdvanced,id:5383,x:32504,y:33447,varname:node_5383,prsc:2|IN-2391-V,IMIN-3281-OUT,IMAX-5745-OUT,OMIN-3664-OUT,OMAX-903-OUT;n:type:ShaderForge.SFN_Vector1,id:3664,x:32260,y:33458,varname:node_3664,prsc:2,v1:-1;n:type:ShaderForge.SFN_Vector1,id:903,x:32324,y:33522,varname:node_903,prsc:2,v1:0.7;proporder:7164-7241-4976-8752;pass:END;sub:END;*/

Shader "Shader Forge/simpleSky" {
    Properties {
        _TOP ("TOP", Color) = (0.1415441,0.1796145,0.5661765,1)
        _ColorMid ("ColorMid", Color) = (0.07843138,0.3921569,0.7843137,1)
        _ColorBottom ("ColorBottom", Color) = (1,1,1,1)
        _BottomColor ("Bottom Color", Range(1, 0)) = 1
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles metal 
            #pragma target 2.0
            uniform float4 _ColorMid;
            uniform float4 _ColorBottom;
            uniform float4 _TOP;
            uniform float _BottomColor;
            struct VertexInput {
                float4 vertex : POSITION;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.pos = UnityObjectToClipPos( v.vertex );
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
////// Lighting:
////// Emissive:
                float node_3833 = 0.0;
                float node_1076 = 0.0;
                float node_3281 = 0.0;
                float node_5745 = 1.0;
                float node_7580 = 0.0;
                float node_3664 = (-1.0);
                float3 emissive = lerp(lerp(_ColorBottom.rgb,_ColorMid.rgb,(node_1076 + ( (i.uv0.g - node_3833) * (_BottomColor - node_1076) ) / (1.0 - node_3833))),_TOP.rgb,((node_7580 + ( (i.uv0.g - node_3281) * (1.0 - node_7580) ) / (node_5745 - node_3281))*(node_3664 + ( (i.uv0.g - node_3281) * (0.7 - node_3664) ) / (node_5745 - node_3281))));
                float3 finalColor = emissive;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
