// Shader created with Shader Forge v1.38 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.38;sub:START;pass:START;ps:flbk:,iptp:0,cusa:False,bamd:0,cgin:,lico:1,lgpr:1,limd:3,spmd:1,trmd:0,grmd:0,uamb:True,mssp:True,bkdf:False,hqlp:False,rprd:False,enco:False,rmgx:True,imps:True,rpth:0,vtps:0,hqsc:True,nrmq:1,nrsp:0,vomd:0,spxs:False,tesm:0,olmd:1,culm:0,bsrc:0,bdst:1,dpts:2,wrdp:True,dith:0,atcv:False,rfrpo:True,rfrpn:Refraction,coma:15,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,stcl:False,atwp:False,stva:128,stmr:255,stmw:255,stcp:6,stps:0,stfa:0,stfz:0,ofsf:0,ofsu:0,f2p0:False,fnsp:False,fnfb:False,fsmp:False;n:type:ShaderForge.SFN_Final,id:3138,x:33152,y:32693,varname:node_3138,prsc:2|diff-1330-OUT,custl-1330-OUT;n:type:ShaderForge.SFN_Color,id:7241,x:31812,y:33139,ptovrint:False,ptlb:Diffuse Color,ptin:_DiffuseColor,varname:node_7241,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:0.07843138,c2:0.3921569,c3:0.7843137,c4:1;n:type:ShaderForge.SFN_Blend,id:1330,x:32838,y:32929,varname:node_1330,prsc:2,blmd:10,clmp:True|SRC-4569-OUT,DST-4731-OUT;n:type:ShaderForge.SFN_Lerp,id:4569,x:32708,y:32598,varname:node_4569,prsc:2|A-4011-RGB,B-9756-OUT,T-5153-OUT;n:type:ShaderForge.SFN_Color,id:4011,x:31821,y:32436,ptovrint:False,ptlb:Shadow_Color,ptin:_Shadow_Color,varname:node_4011,prsc:2,glob:False,taghide:False,taghdr:False,tagprd:False,tagnsco:False,tagnrm:False,c1:1,c2:0,c3:0,c4:1;n:type:ShaderForge.SFN_Vector4,id:9756,x:32422,y:32638,varname:node_9756,prsc:2,v1:0.4,v2:0.4,v3:0.4,v4:1;n:type:ShaderForge.SFN_OneMinus,id:5153,x:32459,y:32828,varname:node_5153,prsc:2|IN-8211-OUT;n:type:ShaderForge.SFN_Multiply,id:8211,x:32290,y:32828,varname:node_8211,prsc:2|A-794-OUT,B-116-OUT;n:type:ShaderForge.SFN_OneMinus,id:794,x:31908,y:32768,varname:node_794,prsc:2|IN-8367-OUT;n:type:ShaderForge.SFN_Vector1,id:116,x:32128,y:32907,varname:node_116,prsc:2,v1:10;n:type:ShaderForge.SFN_LightAttenuation,id:8367,x:31683,y:32810,varname:node_8367,prsc:2;n:type:ShaderForge.SFN_Multiply,id:4731,x:32536,y:33077,varname:node_4731,prsc:2|A-8367-OUT,B-8892-OUT,C-7904-RGB;n:type:ShaderForge.SFN_Blend,id:8892,x:32239,y:33112,varname:node_8892,prsc:2,blmd:10,clmp:True|SRC-4011-RGB,DST-7241-RGB;n:type:ShaderForge.SFN_LightColor,id:7904,x:32396,y:33231,varname:node_7904,prsc:2;proporder:7241-4011;pass:END;sub:END;*/

Shader "Shader Forge/NewShader" {
    Properties {
        _DiffuseColor ("Diffuse Color", Color) = (0.07843138,0.3921569,0.7843137,1)
        _Shadow_Color ("Shadow_Color", Color) = (1,0,0,1)
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        Pass {
            Name "FORWARD"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _DiffuseColor;
            uniform float4 _Shadow_Color;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float4 posWorld : TEXCOORD0;
                float3 normalDir : TEXCOORD1;
                LIGHTING_COORDS(2,3)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.normalDir = UnityObjectToWorldNormal(v.normal);
                o.posWorld = mul(unity_ObjectToWorld, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
                float3 viewDirection = normalize(_WorldSpaceCameraPos.xyz - i.posWorld.xyz);
                float3 normalDirection = i.normalDir;
                float3 viewReflectDirection = reflect( -viewDirection, normalDirection );
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float3 finalColor = 0;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        Pass {
            Name "FORWARD_DELTA"
            Tags {
                "LightMode"="ForwardAdd"
            }
            Blend One One
            
            
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDADD
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #include "Lighting.cginc"
            #include "UnityPBSLighting.cginc"
            #include "UnityStandardBRDF.cginc"
            #pragma multi_compile_fwdadd_fullshadows
            #pragma only_renderers d3d9 d3d11 glcore gles 
            #pragma target 3.0
            uniform float4 _DiffuseColor;
            uniform float4 _Shadow_Color;
            struct VertexInput {
                float4 vertex : POSITION;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                LIGHTING_COORDS(0,1)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                float3 lightColor = _LightColor0.rgb;
                o.pos = UnityObjectToClipPos( v.vertex );
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            float4 frag(VertexOutput i) : COLOR {
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
                float3 finalColor = 0;
                return fixed4(finalColor * 1,0);
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
