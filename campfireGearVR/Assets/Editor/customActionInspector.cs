﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEditor;


[CustomEditor(typeof(actionScript))]

public class customActionInspector : Editor
{


    public override void OnInspectorGUI()
    {
        actionScript script = (actionScript)target;

        script.triggerTime = EditorGUILayout.FloatField("Trigger time", script.triggerTime);
        script.nActions = EditorGUILayout.IntField("number of Actions", script.nActions);
        script.playSourceAudio = EditorGUILayout.Toggle("Play Audio", script.playSourceAudio);
        script.playPlayer2DAudio = EditorGUILayout.Toggle("Play player Audio", script.playPlayer2DAudio);

        for (int i = 0; i < script.nActions; i++)
        {
            int index = i;






            script.Type[i] = (actionScript.actionType)EditorGUILayout.EnumPopup("type", script.Type[i]);








            if (script.Type[i] == actionScript.actionType.levelLoader)
            {
                //script.levelTargetname = EditorGUILayout.TextField("Level name", script.levelTargetname);
                script.locked = EditorGUILayout.Toggle("Level locked", script.locked);
                script.levelIndex = EditorGUILayout.IntField("Level index", script.levelIndex);

                script.hasDelay = EditorGUILayout.Toggle("has delay?", script.hasDelay);

            }




        if (script.Type[i] == actionScript.actionType.travelToZone)
            {


               

                script.targetTravelPosition = (GameObject)EditorGUILayout.ObjectField(script.targetTravelPosition, typeof(GameObject), true);
                script.trackingScale = EditorGUILayout.FloatField("tracking scale at target", script.trackingScale);
            }


           




            if (script.Type[i] == actionScript.actionType.clickButton)
            {
                script.captureButton = (Button)EditorGUILayout.ObjectField(script.captureButton, typeof(Button), true);

            }

            if (script.Type[i] == actionScript.actionType.objectSwitch)
            {

                script.objectToSwitch = (GameObject)EditorGUILayout.ObjectField(script.objectToSwitch, typeof(GameObject), true);

            }
            if (script.Type[i] == actionScript.actionType.tunOff)
            {

                script.objectToOff = (GameObject)EditorGUILayout.ObjectField(script.objectToOff, typeof(GameObject), true);

            }
            if (script.Type[i] == actionScript.actionType.animationTrigger)
            {
                script.animtarget = (GameObject)EditorGUILayout.ObjectField(script.animtarget, typeof(GameObject), true);

                script.animationSpeed = EditorGUILayout.FloatField("Animation speed", script.animationSpeed);
                script.playOnce = EditorGUILayout.Toggle("Play Once", script.playOnce);
                script.isBacknForth = EditorGUILayout.Toggle("is reversed", script.isBacknForth);
                script.loop = EditorGUILayout.Toggle("Loop", script.loop);
                if (script.loop == true)
                {
                    script.loopTime = EditorGUILayout.FloatField("Loop length:", script.loopTime);
                }




            }
            EditorUtility.SetDirty(target);
           // EditorApplication.MarkSceneDirty();

        }
    }

 
}
