2O                         POINT_COOKIE   FOG_EXP2  #ifdef VERTEX
#version 300 es

uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 unity_FogParams;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
in highp vec4 in_POSITION0;
in highp vec3 in_NORMAL0;
in highp vec2 in_TEXCOORD0;
out highp vec2 vs_TEXCOORD0;
out highp float vs_TEXCOORD5;
out highp vec4 vs_TEXCOORD1;
out highp vec3 vs_TEXCOORD2;
out highp vec3 vs_TEXCOORD3;
vec4 u_xlat0;
vec4 u_xlat1;
vec4 u_xlat2;
float u_xlat10;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[3] * in_POSITION0.wwww + u_xlat0;
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
    u_xlat1 = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
    gl_Position = u_xlat1;
    u_xlat1.x = u_xlat1.z * unity_FogParams.x;
    u_xlat1.x = u_xlat1.x * (-u_xlat1.x);
    vs_TEXCOORD5 = exp2(u_xlat1.x);
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy;
    vs_TEXCOORD1 = u_xlat0;
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat10 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat10 = inversesqrt(u_xlat10);
    vs_TEXCOORD2.xyz = vec3(u_xlat10) * u_xlat1.xyz;
    u_xlat1.xyz = u_xlat0.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * u_xlat0.zzz + u_xlat1.xyz;
    vs_TEXCOORD3.xyz = hlslcc_mtx4x4unity_WorldToLight[3].xyz * u_xlat0.www + u_xlat0.xyz;
    return;
}

#endif
#ifdef FRAGMENT
#version 300 es

precision highp int;
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	mediump vec4 _LightColor0;
uniform 	vec4 _Shadow;
uniform 	vec4 _Light;
uniform 	vec4 _Pattern_ST;
uniform 	float _Tile;
uniform 	float _Glossiness;
uniform 	float _Specular;
uniform 	vec4 _specularcolor;
uniform 	vec4 _Diffuseimage_ST;
uniform lowp sampler2D _LightTextureB0;
uniform lowp samplerCube _LightTexture0;
uniform lowp sampler2D _Diffuseimage;
uniform lowp sampler2D _Pattern;
in highp vec2 vs_TEXCOORD0;
in highp float vs_TEXCOORD5;
in highp vec4 vs_TEXCOORD1;
in highp vec3 vs_TEXCOORD2;
in highp vec3 vs_TEXCOORD3;
layout(location = 0) out highp vec4 SV_Target0;
vec4 u_xlat0;
vec3 u_xlat1;
vec3 u_xlat2;
vec2 u_xlat3;
mediump vec3 u_xlat16_3;
lowp vec3 u_xlat10_3;
lowp vec3 u_xlat10_4;
vec2 u_xlat13;
float u_xlat15;
mediump float u_xlat16_15;
lowp float u_xlat10_15;
lowp float u_xlat10_16;
void main()
{
    u_xlat0.xyz = _WorldSpaceLightPos0.www * (-vs_TEXCOORD1.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat15 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat0.xyz = vec3(u_xlat15) * u_xlat0.xyz;
    u_xlat1.xyz = (-vs_TEXCOORD1.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat15 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat1.xyz = u_xlat1.xyz * vec3(u_xlat15) + u_xlat0.xyz;
    u_xlat15 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat1.xyz = vec3(u_xlat15) * u_xlat1.xyz;
    u_xlat15 = dot(vs_TEXCOORD2.xyz, vs_TEXCOORD2.xyz);
    u_xlat15 = inversesqrt(u_xlat15);
    u_xlat2.xyz = vec3(u_xlat15) * vs_TEXCOORD2.xyz;
    u_xlat0.w = dot(u_xlat1.xyz, u_xlat2.xyz);
    u_xlat0.x = dot(u_xlat0.xyz, u_xlat2.xyz);
    u_xlat0.xw = max(u_xlat0.xw, vec2(0.0, 0.0));
    u_xlat0.xyz = u_xlat0.xxx * _Light.xyz;
    u_xlat15 = log2(u_xlat0.w);
    u_xlat15 = u_xlat15 * _Glossiness;
    u_xlat15 = exp2(u_xlat15);
    u_xlat15 = u_xlat15 * _Specular;
    u_xlat1.xyz = vec3(u_xlat15) * _specularcolor.xyz;
    u_xlat15 = dot(vs_TEXCOORD3.xyz, vs_TEXCOORD3.xyz);
    u_xlat10_15 = texture(_LightTextureB0, vec2(u_xlat15)).w;
    u_xlat10_16 = texture(_LightTexture0, vs_TEXCOORD3.xyz).w;
    u_xlat16_15 = u_xlat10_15 * u_xlat10_16;
    u_xlat2.xyz = (-_Shadow.xyz) + _Light.xyz;
    u_xlat2.xyz = vec3(u_xlat16_15) * u_xlat2.xyz + _Shadow.xyz;
    u_xlat15 = float(1.0) / _Tile;
    u_xlat3.xy = vs_TEXCOORD0.xy + vec2(0.0, 1.0);
    u_xlat3.xy = vec2(u_xlat15) * u_xlat3.xy;
    u_xlat13.xy = u_xlat3.xy * _Pattern_ST.xy + _Pattern_ST.zw;
    u_xlat3.xy = u_xlat3.xy * _Diffuseimage_ST.xy + _Diffuseimage_ST.zw;
    u_xlat10_4.xyz = texture(_Diffuseimage, u_xlat3.xy).xyz;
    u_xlat10_3.xyz = texture(_Pattern, u_xlat13.xy).xyz;
    u_xlat16_3.xyz = u_xlat10_3.xyz * vec3(1.5, 1.5, 1.5);
#ifdef UNITY_ADRENO_ES3
    u_xlat16_3.xyz = min(max(u_xlat16_3.xyz, 0.0), 1.0);
#else
    u_xlat16_3.xyz = clamp(u_xlat16_3.xyz, 0.0, 1.0);
#endif
    u_xlat16_3.xyz = (-u_xlat16_3.xyz) + vec3(1.0, 1.0, 1.0);
    u_xlat1.xyz = u_xlat16_3.xyz * u_xlat1.xyz + u_xlat2.xyz;
    u_xlat1.xyz = u_xlat1.xyz * u_xlat10_4.xyz;
    u_xlat1.xyz = u_xlat1.xyz * _LightColor0.xyz;
    u_xlat0.xyz = u_xlat0.xyz * u_xlat1.xyz;
    u_xlat15 = vs_TEXCOORD5;
#ifdef UNITY_ADRENO_ES3
    u_xlat15 = min(max(u_xlat15, 0.0), 1.0);
#else
    u_xlat15 = clamp(u_xlat15, 0.0, 1.0);
#endif
    SV_Target0.xyz = u_xlat0.xyz * vec3(u_xlat15);
    SV_Target0.w = 0.0;
    return;
}

#endif
                             